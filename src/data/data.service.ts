import { BadRequestException, Injectable } from '@nestjs/common';
import employeesData from '../../data/employees';
import ordersData from '../../data/orders';
import vouchersData from '../../data/vouchers';
import partnerData from '../../data/partners';
import companiesData from '../../data/companies';
import { DEFAULT_REMAINING_BUDGET } from '../constants';
import { EmployeeModel } from '../employees/models/employee.model';
import {
  BudgetData,
  Company,
  Employee,
  IDable,
  ListEmployeeArgs,
  Order,
  Partner,
  Voucher,
} from '../types';
import { RevenueModel } from '../partners/models/revenue.model';

/**
 * On the real world this would probably be spread along ORM
 * And concerns, for these mocks we leave it all together
 */
@Injectable()
export class DataService {
  /**
   * Lists a company employees with their budgets within the month
   */
  listEmployeesByCompany({
    year,
    month,
    companyId,
    minRemainingBudget = DEFAULT_REMAINING_BUDGET,
  }: ListEmployeeArgs): EmployeeModel[] {
    if (year && !month) {
      throw new BadRequestException(
        `Listing employees with a year and without a month is not possible`,
      );
    }
    const currentDate = new Date();
    const targetYear = year || currentDate.getFullYear();
    const targetMonth = month || currentDate.getMonth() + 1;

    const employees = this.getEmployeesByCompanyId(companyId);

    const { employeeIds, employeeMap, voucherMap } =
      this.getDataUtilities(employees);

    const orders = this.getEmployeeOrders(employeeIds, targetYear, targetMonth);

    const employeeIdVoucherMap = this.getEmployeeIdVoucherMap(orders);

    const employeeTotalMap = this.getEmployeesExpensesAndBudget(
      minRemainingBudget,
      employeeIdVoucherMap,
      employeeMap,
      voucherMap,
    );

    return Object.entries(employeeTotalMap).map(([employeeId, budgetData]) =>
      this.consolidateEmployeeModel(employeeMap[employeeId], budgetData),
    );
  }

  /**
   * Make the proper data shape of an employee and it's budget info
   * @param employee
   * @param budgetInfo
   * @returns
   */
  consolidateEmployeeModel(
    employee: Employee,
    budgetInfo: BudgetData,
  ): EmployeeModel {
    return {
      ...employee,
      remainingBudget: budgetInfo.remaining,
      expentBudget: budgetInfo.expent,
    };
  }

  /**
   * makes a map with all employee and it's used vouchers
   * @param orders
   * @returns
   */
  getEmployeeIdVoucherMap(orders: Order[]) {
    return orders.reduce<Record<string, number[]>>(
      (orderMap, { employeeId, voucherId }) => ({
        ...orderMap,
        [employeeId]: orderMap[employeeId]?.concat(voucherId) || [voucherId],
      }),
      {},
    );
  }

  /**
   * Utility function, we will need these entities frequently
   */
  getDataUtilities(employees: Employee[]) {
    const employeeMap = DataService.makeMapById(employees);
    const employeeIds = employees.map(({ id }) => id);
    const voucherMap = DataService.makeMapById(vouchersData);

    return { employeeMap, employeeIds, voucherMap };
  }

  /**
   * Filters employees with at least target budget
   * @param targetBudget
   * @param employeeVoucherMap
   * @param employeeMap
   * @param voucherMap
   * @returns a map of employees with ther BudgetData
   */
  getEmployeesExpensesAndBudget(
    targetBudget: number,
    employeeVoucherMap: Record<string, number[]>,
    employeeMap: Record<string, Employee>,
    voucherMap: Record<string, Voucher>,
  ) {
    return Object.keys(employeeMap).reduce<Record<string, BudgetData>>(
      (totalMap, employeeId) => {
        let expent = 0;
        const voucherIds = employeeVoucherMap[employeeId];
        if (voucherIds) {
          expent = this.getVoucherSum(voucherIds, voucherMap);
        }

        const remaining = employeeMap[employeeId].budget - expent;
        if (remaining >= targetBudget) {
          return { ...totalMap, [employeeId]: { remaining, expent } };
        }

        return totalMap;
      },
      {},
    );
  }

  /**
   * Sum the expending of the list of vouchers
   * @param voucherIds
   * @param voucherMap
   * @returns
   */
  getVoucherSum(
    voucherIds: number[],
    voucherMap: Record<string, Voucher>,
  ): number {
    return voucherIds.reduce((total, id) => total + voucherMap[id].amount, 0);
  }

  /**
   * Filter employees of a given company
   * @param companyId
   * @returns
   */
  getEmployeesByCompanyId(companyId: number) {
    return employeesData.filter(
      ({ companyId: employeeCompanyId }) => companyId === employeeCompanyId,
    );
  }

  /**
   * Filter employees order for a given year and month
   * @param employeeIds allowed employees
   * @param year
   * @param month
   * @returns
   */
  getEmployeeOrders(employeeIds: number[], year: number, month: number) {
    return ordersData.filter(({ date, employeeId }) => {
      return (
        employeeIds.includes(employeeId) &&
        date.getFullYear() === year &&
        date.getMonth() + 1 === month
      );
    });
  }

  getCompanies(): Company[] {
    return companiesData;
  }

  getPartners(): Partner[] {
    return partnerData;
  }

  /**
   * List partners revenue withing a time range if no range is passed current start and end of month are used
   * @returns
   */
  getPartnerRevenue({
    partnerId,
    startDate,
    endDate,
  }: {
    partnerId: number;
    startDate?: string;
    endDate?: string;
  }): RevenueModel {
    const refDate = new Date();
    const firstDayCurrentMonth = new Date(
      refDate.getFullYear(),
      refDate.getMonth(),
      1,
    );
    const lastDayCurrentMonth = new Date(
      refDate.getFullYear(),
      refDate.getMonth() + 1,
      0,
    );
    const start = startDate ? new Date(startDate) : firstDayCurrentMonth;
    const end = endDate ? new Date(endDate) : lastDayCurrentMonth;

    const vouchers = this.getPartnerVouchers(partnerId);
    const voucherMap = DataService.makeMapById(vouchers);
    const orderVoucherId = vouchers.map(({ id }) => id);

    const orders = this.getPartnerOrders({
      voucherIds: orderVoucherId,
      start,
      end,
    });
    const voucherIds = orders.map(({ voucherId }) => voucherId);
    const total = this.getVoucherSum(voucherIds, voucherMap);
    return {
      total,
    };
  }

  /**
   * Filters vouchers to a given partner
   * @param partnerId
   * @returns
   */
  getPartnerVouchers(partnerId: number) {
    return vouchersData.filter(
      ({ partnerId: voucherPartner }) => voucherPartner === partnerId,
    );
  }
  /**
   * Get orders within allowed vouchers and range
   * @returns
   */
  getPartnerOrders({
    voucherIds,
    start,
    end,
  }: {
    voucherIds: number[];
    start: Date;
    end: Date;
  }) {
    return ordersData.filter(
      ({ voucherId: orderVoucher, date }) =>
        voucherIds.includes(orderVoucher) && date >= start && date < end,
    );
  }

  /**
   * Utility function for map creation
   * @param data
   * @returns
   */
  static makeMapById<T extends IDable>(data: T[]) {
    return data.reduce<Record<string, T>>((map, item) => {
      return { ...map, [item.id]: item };
    }, {});
  }
}

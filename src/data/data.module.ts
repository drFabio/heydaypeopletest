import { Module } from '@nestjs/common';
import { DataService } from './data.service';

/**
 * Data abstraction module
 */
@Module({
  providers: [DataService],
  exports: [DataService],
})
export class DataModule {}

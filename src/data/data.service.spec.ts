import { BadRequestException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { DEFAULT_REMAINING_BUDGET } from '../constants';
import { BudgetData, Employee } from '../types';
import { DataService } from './data.service';
/**
 * NOTE: Of course in real life we would be testing a database access
 * Anyhow we prepare a fully isolated test here
 */
describe('DataService', () => {
  let service: DataService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DataService],
    }).compile();
    mockEmployeeData = require('../../data/employees');
    mockOrdersData = require('../../data/orders');
    mockVouchersData = require('../../data/vouchers');
    service = module.get<DataService>(DataService);
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe(`getEmployeesByCompanyId`, () => {
    it(`Filter employees by company id`, () => {
      mockEmployeeData.filter.mockImplementation(() => mockResult);
      expect(service.getEmployeesByCompanyId(mockCompanyId)).toEqual(
        mockResult,
      );
      // the function that was passed to the filter
      const [asserter] = mockEmployeeData.filter.mock.calls[0];
      expect(asserter({ companyId: mockCompanyId })).toBe(true);
    });
  });

  describe('getEmployeeIdVoucherMap', () => {
    it(`Sums all voucher ids for a given employer`, () => {
      const mockOrders = { reduce: jest.fn((..._args) => mockResult) };
      expect(service.getEmployeeIdVoucherMap(mockOrders as any)).toEqual(
        mockResult,
      );
      const [reducer] = mockOrders.reduce.mock.calls[0];
      const mockOrderMap = { [mockTargetEmployeeId]: [23] };
      const mockVoucherId = 53;
      expect(
        reducer(mockOrderMap, {
          employeeId: mockTargetEmployeeId,
          voucherId: mockVoucherId,
        }),
      ).toEqual({ [mockTargetEmployeeId]: [23, mockVoucherId] });
      const newEmployeeId = 61;
      expect(
        reducer(mockOrderMap, {
          employeeId: newEmployeeId,
          voucherId: mockVoucherId,
        }),
      ).toEqual({
        [mockTargetEmployeeId]: [23],
        [newEmployeeId]: [mockVoucherId],
      });
    });
  });

  describe(`getEmployeeOrders`, () => {
    it(`Filters by employeeId, year and month`, () => {
      mockOrdersData.filter.mockImplementation(() => mockResult);
      const employeeOutsideOfIds = 13;
      const mockEmmployeeIds = [3, 7];
      const employeeInsideId = mockEmmployeeIds[0];
      expect(
        service.getEmployeeOrders(mockEmmployeeIds, mockYear, mockMonth),
      ).toEqual(mockResult);
      const [asserter] = mockOrdersData.filter.mock.calls[0];
      expect(
        asserter({
          employeeId: employeeInsideId,
          date: new Date(mockYear, mockMonth - 1),
        }),
      ).toBe(true);
      expect(
        asserter({
          employeeId: employeeOutsideOfIds,
          date: new Date(mockYear, mockMonth - 1),
        }),
      ).toBe(false);
      expect(
        asserter({
          employeeId: employeeInsideId,
          date: new Date(mockYear + 1, mockMonth - 1),
        }),
      ).toBe(false);
      expect(
        asserter({
          mockEmployeeId: employeeInsideId,
          date: new Date(mockYear, mockMonth - 2),
        }),
      ).toBe(false);
    });
  });

  describe(`getDataUtilities`, () => {
    let mockMakeMapById: jest.Mock;
    beforeEach(() => {
      mockMakeMapById = jest.spyOn(DataService, 'makeMapById') as jest.Mock;
    });
    it(`Returns the map and id list`, () => {
      const mockVoucherMap = Symbol('mockVoucherMap');

      const mockEmployees = {
        map: jest.fn((..._args) => mockEmployeeIds),
      };
      mockMakeMapById.mockImplementationOnce(() => mockEmployeeMap);
      mockMakeMapById.mockImplementationOnce(() => mockVoucherMap);

      expect(service.getDataUtilities(mockEmployees as any)).toEqual({
        employeeMap: mockEmployeeMap,
        employeeIds: mockEmployeeIds,
        voucherMap: mockVoucherMap,
      });
      const [mapper] = mockEmployees.map.mock.calls[0];
      const mockCurrentEmployee = { id: mockTargetEmployeeId };

      expect(mapper(mockCurrentEmployee)).toEqual(mockTargetEmployeeId);
    });
  });

  describe('consolidateEmployeeModel', () => {
    it(`Maps properly`, () => {
      const mockRemaining = 31;
      const mockExpent = 181;
      const mockBudgetData: BudgetData = {
        remaining: mockRemaining,
        expent: mockExpent,
      };

      expect(
        service.consolidateEmployeeModel(mockEmployee as any, mockBudgetData),
      ).toEqual({
        ...mockEmployee,
        remainingBudget: mockRemaining,
        expentBudget: mockExpent,
      });
    });
  });
  describe('getEmployeesExpensesAndBudget', () => {
    it(`returns a map with the remainign budget of the ones equal or above target budget`, () => {
      const mockReducer = jest.fn((..._args) => mockResult);
      const originalKeys = Object.keys;
      jest.spyOn(Object, 'keys').mockImplementation((...arg) => {
        if (arg[0] === mockEmployeeMap) {
          return { reduce: mockReducer } as any;
        }
        return originalKeys(...arg);
      });
      jest
        .spyOn(service, 'getVoucherSum')
        .mockImplementation(() => mockSumValue);

      const mockVoucherIds = Symbol('mockVoucherIds');

      const mockEmployeeVoucherMap = {
        [mockTargetEmployeeId]: mockVoucherIds,
      } as any;
      const mockFailingEmployeeId = 23;

      const mockFailingEmployee: Employee = {
        budget: failingBudget,
      } as unknown as Employee;
      const mockEmployeeMap = {
        [mockTargetEmployeeId]: mockEmployee as unknown as Employee,
        [mockFailingEmployeeId]: mockFailingEmployee,
      };
      const mockVoucherMap = Symbol('mockVoucherMap');
      expect(
        service.getEmployeesExpensesAndBudget(
          mockTargetBudget,
          mockEmployeeVoucherMap,
          mockEmployeeMap,
          mockVoucherMap as any,
        ),
      ).toEqual(mockResult);

      const [reducer] = mockReducer.mock.calls[0];

      const initialTotalMap = { 29: { expent: 29, remaining: 41 } };

      expect(reducer(initialTotalMap, mockTargetEmployeeId)).toEqual({
        ...initialTotalMap,
        [mockTargetEmployeeId]: {
          remaining: passingBudget - mockSumValue,
          expent: mockSumValue,
        },
      });
      // does not add the failing one
      expect(reducer(initialTotalMap, mockFailingEmployeeId)).toEqual(
        initialTotalMap,
      );
    });
  });
  describe('getVoucherSum', () => {
    it(`Sums by voucherId value only if the voucher is on the list`, () => {
      const mockVoucher1 = 31;
      const mockVoucher2 = 41;
      const mockVoucher1Price = 7;
      const mockVoucher2Price = 11;
      const mockVoucherIds = [
        mockVoucher1,
        mockVoucher1,
        mockVoucher1,
        mockVoucher2,
        mockVoucher2,
      ];
      const voucherMap = {
        [mockVoucher1]: { amount: mockVoucher1Price },
        [mockVoucher2]: { amount: mockVoucher2Price },
      };
      expect(service.getVoucherSum(mockVoucherIds, voucherMap as any)).toEqual(
        3 * mockVoucher1Price + 2 * mockVoucher2Price,
      );
    });
  });
  describe('listEmployeesByCompany', () => {
    beforeEach(() => {
      jest
        .spyOn(global, 'Date')
        .mockImplementation(() => mockDate as unknown as string);
      jest
        .spyOn(service, 'getEmployeesByCompanyId')
        .mockImplementation(() => mockEmployees as any);
      jest.spyOn(service, 'getDataUtilities').mockImplementation(
        () =>
          ({
            employeeIds: mockEmployeeIds,
            employeeMap: mockEmployeeMap,
            voucherMap: mockVoucherMap,
          } as any),
      );
      jest
        .spyOn(service, 'getEmployeeOrders')
        .mockImplementation(() => mockOrdersResponse as any);
      jest
        .spyOn(service, 'getEmployeeIdVoucherMap')
        .mockImplementation(() => mockEmployeeIdVoucherMap as any);
      jest
        .spyOn(service, 'getEmployeesExpensesAndBudget')
        .mockImplementation(() => mockEmployeeTotalMap as any);
      jest
        .spyOn(service, 'consolidateEmployeeModel')
        .mockImplementation(() => mockConsolidateEmployeeModel as any);
    });

    it(`Lists properly `, () => {
      /**
       *   Testing that the internal methods are called with the appropriate arguments
       *
       */
      const targetYear = mockYear + 1;
      const targetMonth = 1;
      const mockRemainingBudget = 61;
      expect(
        service.listEmployeesByCompany({
          year: targetYear,
          month: targetMonth,
          companyId: mockCompanyId,
          minRemainingBudget: mockRemainingBudget,
        }),
      ).toEqual([mockConsolidateEmployeeModel]);

      expect(service.getEmployeesByCompanyId as jest.Mock).toHaveBeenCalledWith(
        mockCompanyId,
      );
      expect(service.getDataUtilities as jest.Mock).toHaveBeenCalledWith(
        mockEmployees,
      );

      expect(service.getEmployeeOrders as jest.Mock).toHaveBeenCalledWith(
        mockEmployeeIds,
        targetYear,
        targetMonth,
      );

      expect(service.getEmployeeIdVoucherMap).toHaveBeenCalledWith(
        mockOrdersResponse,
      );

      expect(service.getEmployeesExpensesAndBudget).toHaveBeenCalledWith(
        mockRemainingBudget,
        mockEmployeeIdVoucherMap,
        mockEmployeeMap,
        mockVoucherMap,
      );

      expect(service.consolidateEmployeeModel).toHaveBeenCalledWith(
        mockEmployee,
        mockTotalForEmployeeId,
      );
    });
    it(`Defaults to current year and month if absent`, () => {
      service.listEmployeesByCompany({
        companyId: mockCompanyId,
      });
      // Values of the global date that was mocked
      expect(service.getEmployeeOrders as jest.Mock).toHaveBeenCalledWith(
        mockEmployeeIds,
        mockYear,
        mockMonth,
      );
    });
    it(`Defaults to constant budget`, () => {
      service.listEmployeesByCompany({
        companyId: mockCompanyId,
      });
      // Values of the global date that was mocked
      expect(
        service.getEmployeesExpensesAndBudget as jest.Mock,
      ).toHaveBeenCalledWith(
        DEFAULT_REMAINING_BUDGET,
        mockEmployeeIdVoucherMap,
        mockEmployeeMap,
        mockVoucherMap,
      );
    });

    it(`Throws an exception if trying to list employees on a year without a month`, () => {
      expect(() => {
        service.listEmployeesByCompany({
          companyId: mockCompanyId,
          year: mockYear,
        });
      }).toThrow(BadRequestException);
    });
  });
  describe('getPartnerOrders', () => {
    let mockVoucherList: { includes: jest.Mock };
    beforeEach(() => {
      mockOrdersData.filter.mockImplementation(() => mockResult);

      mockVoucherList = { includes: jest.fn() };
    });
    it('returns filter answer', () => {
      expect(
        service.getPartnerOrders({
          voucherIds: mockVoucherList as any,
          start: mockStartDate,
          end: mockEndDate,
        }),
      ).toEqual(mockResult);
    });
    describe('assertions', () => {
      let asserter: (...args: any[]) => boolean;
      const mockVoucherId = Symbol('mockVoucherId');

      beforeEach(() => {
        service.getPartnerOrders({
          voucherIds: mockVoucherList as any,
          start: mockStartDate,
          end: mockEndDate,
        }),
          ([asserter] = mockOrdersData.filter.mock.calls[0]);
      });
      it('Returns true if within id and range', () => {
        mockVoucherList.includes.mockImplementationOnce(() => true);
        expect(asserter({ voucherId: mockVoucherId, date: mockDate })).toBe(
          true,
        );
      });
      it('does not return true if not on id', () => {
        mockVoucherList.includes.mockImplementationOnce(() => false);
        expect(asserter({ voucherId: mockVoucherId, date: mockDate })).toBe(
          false,
        );
      });
      it('does not return true if before start date', () => {
        mockVoucherList.includes.mockImplementationOnce(() => false);
        expect(
          asserter({
            voucherId: mockVoucherId,
            date: new Date(mockStartDate.getTime() - 1),
          }),
        ).toBe(false);
      });
      it('does not return  true if exactly on end date or after', () => {
        mockVoucherList.includes.mockImplementationOnce(() => false);
        expect(
          asserter({
            voucherId: mockVoucherId,
            date: mockEndDate,
          }),
        ).toBe(false);
      });
    });
  });
  it.todo('getPartnerRevenue');
  it.todo('getCompanies');
  it.todo('makeMapById');
});

/**
 * Mock data at the end so we don't uglify the test
 */

let mockEmployeeData: { filter: jest.Mock; reduce: jest.Mock; map: jest.Mock };
let mockOrdersData: { filter: jest.Mock };
let mockVouchersData: { reduce: jest.Mock };

const mockYear = 2020;
const mockCompanyId = 13;
const mockMonth = 11;
const mockDate = new Date(mockYear, mockMonth - 1, 1, 0, 0, 0);
const mockStartDate = new Date(mockYear - 10, mockMonth - 1, 1, 0, 0, 0);

const mockEndDate = new Date(mockYear + 10, mockMonth - 1, 1, 0, 0, 0);

const mockEmployeeIds = Symbol('mockMap');
const mockResult = Symbol('mockResult');
const mockTargetEmployeeId = 27;
const mockTargetBudget = 10;
const mockSumValue = 5;
const passingBudget = mockTargetBudget + mockSumValue;
const failingBudget = mockTargetBudget - 1;
const mockEmployee = { id: Symbol('mockId'), budget: passingBudget };
const mockEmployeeMap = { [mockTargetEmployeeId]: mockEmployee };

const mockEmployees = {
  map: jest.fn(),
};
const mockEmployeeIdVoucherMap = Symbol('employeeIdVoucherMap');
const mockOrdersResponse = Symbol('mockOrdersResponse');
const mockTotalForEmployeeId = Symbol('mockTotalForEmployeeId');
const mockVoucherMap = Symbol('mockVoucherMap');
const mockEmployeeTotalMap = {
  [mockTargetEmployeeId]: mockTotalForEmployeeId,
};
const mockConsolidateEmployeeModel = Symbol('mockConsolidateEmployeeModel');

jest.mock('../../data/employees', () => ({
  filter: jest.fn(),
  reduce: jest.fn(),
  map: jest.fn(),
}));
jest.mock('../../data/orders', () => ({
  filter: jest.fn(),
}));
jest.mock('../../data/vouchers', () => ({
  reduce: jest.fn(),
}));

import { join } from 'path';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { AppResolver } from '@src/app.resolver';
import { EmployeesModule } from './employees/employees.module';
import { DataModule } from './data/data.module';
import { CompanyEmployeesResolver } from './employees/company-employees.resolver';
import { TaxesModule } from './taxes/taxes.module';
import { PartnersModule } from './partners/partners.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    }),
    EmployeesModule,
    DataModule,
    TaxesModule,
    PartnersModule,
  ],
  providers: [AppResolver, CompanyEmployeesResolver],
})
export class AppModule {}

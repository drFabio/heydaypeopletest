import { Test, TestingModule } from '@nestjs/testing';
import { DataService } from '../data/data.service';
import { CompanyEmployeesResolver } from './company-employees.resolver';

describe('CompanyEmployeesResolver', () => {
  let resolver: CompanyEmployeesResolver;
  let mockDataService: { listEmployeesByCompany: jest.Mock };
  beforeEach(async () => {
    mockDataService = { listEmployeesByCompany: jest.fn() };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { provide: DataService, useValue: mockDataService },

        CompanyEmployeesResolver,
      ],
    }).compile();

    resolver = module.get<CompanyEmployeesResolver>(CompanyEmployeesResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});

import { Test, TestingModule } from '@nestjs/testing';
import { DataService } from '../data/data.service';
import { TaxesService } from '../taxes/taxes.service';
import { EmployeesResolver } from './employees.resolver';
import { EmployeeModel } from './models/employee.model';

describe('EmployeesResolver', () => {
  let resolver: EmployeesResolver;
  let mockDataService: {
    getCompanies: jest.Mock;
    listEmployeesByCompany: jest.Mock;
  };
  let mockTaxesService: {
    getExpenses: jest.Mock;
  };
  const mockResult = Symbol('mockResult');

  const mockMonth = 11;
  const mockCompanyId = 3;
  const mockYear = 2017;
  const mockExpentBudget = 43;
  const mockCompanyResult = [
    {
      someValue: mockResult,
    },
  ];
  const mockEmployeeModel = {
    expentBudget: mockExpentBudget,
  } as unknown as EmployeeModel;

  beforeEach(async () => {
    mockDataService = {
      getCompanies: jest.fn(() => mockCompanyResult),
      listEmployeesByCompany: jest.fn(() => mockResult),
    };
    mockTaxesService = {
      getExpenses: jest.fn(() => mockResult),
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EmployeesResolver,
        { provide: DataService, useValue: mockDataService },
        { provide: TaxesService, useValue: mockTaxesService },
      ],
    }).compile();

    resolver = module.get<EmployeesResolver>(EmployeesResolver);
  });

  it('Lists companiesEmployees', () => {
    const result = resolver.companiesEmployees(mockMonth, mockYear);
    expect(result[0]).toEqual(
      expect.objectContaining({ month: mockMonth, year: mockYear }),
    );
    expect(mockDataService.getCompanies).toHaveBeenCalled();
  });

  it(`lists employees withing a single company`, () => {
    expect(resolver.employees(mockCompanyId, mockMonth, mockYear)).toEqual(
      mockResult,
    );
    expect(mockDataService.listEmployeesByCompany).toHaveBeenCalledWith({
      companyId: mockCompanyId,
      minRemainingBudget: Number.NEGATIVE_INFINITY,
      year: mockYear,
      month: mockMonth,
    });
  });

  it(`Resolves expense field`, () => {
    expect(resolver.expenses(mockEmployeeModel)).toBe(mockResult);

    expect(mockTaxesService.getExpenses).toHaveBeenLastCalledWith(
      mockExpentBudget,
    );
  });
});

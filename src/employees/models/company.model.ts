import { Field, ObjectType, ID } from '@nestjs/graphql';
import { EmployeeModel } from './employee.model';

@ObjectType('Company')
export class CompanyModel {
  @Field(() => ID)
  id: number;

  @Field()
  title: string;

  @Field(() => [EmployeeModel], { nullable: true })
  employees: EmployeeModel[];
}

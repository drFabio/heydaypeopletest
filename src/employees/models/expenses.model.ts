import { ObjectType, Float, Field } from '@nestjs/graphql';

// A list of employees from a single company with their spending in a certain month. It should list the money per employee that was spent up to 44€ as tax free and the money above this threshold should be split up by net salary and taxes. There should also be a total per employee.
@ObjectType('Expenses')
export class ExpensesModel {
  @Field(() => Float)
  taxFree: number;
  @Field(() => Float)
  net: number;

  @Field(() => Float)
  taxes: number;

  @Field(() => Float)
  total: number;
}

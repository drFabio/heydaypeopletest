import { Field, ObjectType, ID, Int } from '@nestjs/graphql';
import { Employee } from '../../types';
import { ExpensesModel } from './expenses.model';

@ObjectType('Employee')
export class EmployeeModel implements Employee {
  @Field(() => ID)
  id: number;

  @Field()
  name: string;

  @Field(() => Int, { description: 'Allocated budget per month' })
  budget: number;

  @Field(() => Int)
  companyId: number;

  @Field(() => Int, { description: 'Amount of remaining budget if any' })
  remainingBudget: number;

  @Field(() => Int, { description: 'Amount of spent budget if any' })
  expentBudget: number;

  @Field(() => ExpensesModel, { nullable: true })
  expenses?: ExpensesModel;
}

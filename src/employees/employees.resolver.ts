import {
  Args,
  Int,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { DataService } from '../data/data.service';
import { TaxesService } from '../taxes/taxes.service';
import { CompanyModel } from './models/company.model';
import { EmployeeModel } from './models/employee.model';
import { ExpensesModel } from './models/expenses.model';

@Resolver(() => EmployeeModel)
export class EmployeesResolver {
  constructor(
    protected readonly dataService: DataService,
    protected readonly taxesService: TaxesService,
  ) {}

  @Query(() => [CompanyModel], {
    description: 'List all employees by company with more than 10€',
  })
  companiesEmployees(
    @Args('month', { type: () => Int, nullable: true }) month: number,
    @Args('year', { type: () => Int, nullable: true }) year: number,
  ) {
    return (
      this.dataService
        .getCompanies()
        // The map here is to enrich the data for the field resolver, that would need the year and month downstream
        .map((c) => ({ ...c, month, year }))
    );
  }

  @Query(() => [EmployeeModel], {
    description: 'List employees within a single company',
  })
  employees(
    @Args('companyId', { type: () => Int }) companyId: number,
    @Args('month', { type: () => Int, nullable: true }) month: number,
    @Args('year', { type: () => Int, nullable: true }) year: number,
  ) {
    return this.dataService.listEmployeesByCompany({
      companyId,
      minRemainingBudget: Number.NEGATIVE_INFINITY,
      year,
      month,
    });
  }

  @ResolveField(() => ExpensesModel)
  expenses(
    @Parent()
    { expentBudget }: EmployeeModel,
  ): ExpensesModel {
    return this.taxesService.getExpenses(expentBudget);
  }
}

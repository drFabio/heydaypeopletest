import { Module } from '@nestjs/common';
import { DataModule } from '../data/data.module';
import { EmployeesResolver } from './employees.resolver';
import { CompanyEmployeesResolver } from './company-employees.resolver';
import { TaxesModule } from '../taxes/taxes.module';

/**
 * Handles all employee related logic
 */
@Module({
  providers: [EmployeesResolver, CompanyEmployeesResolver],
  imports: [DataModule, TaxesModule],
})
export class EmployeesModule {}

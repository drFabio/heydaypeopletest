import { Parent, ResolveField, Resolver } from '@nestjs/graphql';
import { DataService } from '../data/data.service';
import { CompanyModel } from './models/company.model';
import { EmployeeModel } from './models/employee.model';

@Resolver(() => CompanyModel)
export class CompanyEmployeesResolver {
  constructor(protected readonly dataService: DataService) {}

  @ResolveField()
  employees(
    @Parent() parentData: CompanyModel & { month?: number; year?: number },
  ): EmployeeModel[] {
    const { month, year, id: companyId } = parentData;
    return this.dataService.listEmployeesByCompany({
      companyId: +companyId,
      year,
      month,
    });
  }
}

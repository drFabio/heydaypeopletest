/**
 * The budget desired to list the employees
 */
export const DEFAULT_REMAINING_BUDGET = 10;
export const TAX_FREE_VALUE = 44;
export const TAX_RATE = 0.3;

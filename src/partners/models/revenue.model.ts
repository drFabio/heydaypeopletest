import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType('Revenue')
export class RevenueModel {
  @Field()
  total: number;
}

import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Partner } from '../../types';
import { RevenueModel } from './revenue.model';

@ObjectType('Partner')
export class PartnersModel implements Partner {
  @Field(() => ID)
  id: number;

  @Field()
  name: string;

  @Field(() => RevenueModel)
  revenue: RevenueModel;
}

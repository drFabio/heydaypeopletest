import { Module } from '@nestjs/common';
import { DataModule } from '../data/data.module';
import { PartnersResolver } from './partners.resolver';

/**
 * Centralizes partner resolvers
 */
@Module({
  providers: [PartnersResolver],
  imports: [DataModule],
})
export class PartnersModule {}

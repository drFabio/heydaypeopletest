import { Test, TestingModule } from '@nestjs/testing';
import { DataService } from '../data/data.service';
import { PartnersModel } from './models/partners.model';
import { PartnersResolver } from './partners.resolver';

describe('PartnersResolver', () => {
  let resolver: PartnersResolver;
  let mockDataService: { getPartnerRevenue: jest.Mock; getPartners: jest.Mock };

  beforeEach(async () => {
    mockDataService = {
      getPartnerRevenue: jest.fn(() => mockResult),
      getPartners: jest.fn(() => mockResult),
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PartnersResolver,
        {
          provide: DataService,
          useValue: mockDataService,
        },
      ],
    }).compile();

    resolver = module.get<PartnersResolver>(PartnersResolver);
  });

  it(`Get's partner`, () => {
    expect(resolver.partners()).toBe(mockResult);
    expect(mockDataService.getPartners).toHaveBeenCalled();
  });
  it(`resolves revenue`, () => {
    expect(resolver.revenue(mockPartner, mockStartDate, mockEndDate)).toBe(
      mockResult,
    );
    expect(mockDataService.getPartnerRevenue).toHaveBeenCalledWith({
      partnerId: mockParterId,
      startDate: mockStartDate,
      endDate: mockEndDate,
    });
  });
});

const mockResult = Symbol('mockResult');
const mockStartDate = 'mockStartDate';
const mockEndDate = 'mockEndDate';
const mockParterId = 13;
const mockPartner = {
  id: mockParterId,
} as unknown as PartnersModel;

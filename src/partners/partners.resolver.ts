import { Args, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { DataService } from '../data/data.service';
import { Partner } from '../types';
import { PartnersModel } from './models/partners.model';

@Resolver(() => PartnersModel)
export class PartnersResolver {
  constructor(protected readonly dataService: DataService) {}

  @Query(() => [PartnersModel], {
    description: 'List partners revenue within a range',
  })
  partners(): Partner[] {
    return this.dataService.getPartners();
  }

  @ResolveField()
  revenue(
    @Parent() { id: partnerId }: PartnersModel,
    @Args('startDate', { nullable: true }) startDate: string,
    @Args('endDate', { nullable: true }) endDate: string,
  ) {
    return this.dataService.getPartnerRevenue({
      partnerId,
      startDate,
      endDate,
    });
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { TaxesService } from './taxes.service';

describe('TaxesService', () => {
  let service: TaxesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaxesService],
    }).compile();

    service = module.get<TaxesService>(TaxesService);
  });

  it('Works up to the amount', () => {
    expect(service.getExpenses(mockTaxFree, mockTaxFree, mockTaxRate)).toEqual({
      taxFree: mockTaxFree,
      net: 0,
      taxes: 0,
      total: mockTaxFree,
    });
  });
  it('Works with taxes', () => {
    const expectedNet = mockAmount - mockTaxFree;
    const expectedTaxes = parseFloat((expectedNet * mockTaxRate).toFixed(2));
    const expectedTotal = mockAmount + expectedTaxes;
    expect(service.getExpenses(mockAmount, mockTaxFree, mockTaxRate)).toEqual({
      taxFree: mockTaxFree,
      net: expectedNet,
      taxes: expectedTaxes,
      total: expectedTotal,
    });
  });
});

const mockTaxFree = 43;
const mockTaxRate = 0.3;
const mockAmount = 149;

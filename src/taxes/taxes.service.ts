import { Injectable } from '@nestjs/common';
import { TAX_FREE_VALUE, TAX_RATE } from '../constants';
import { ExpensesModel } from '../employees/models/expenses.model';

@Injectable()
export class TaxesService {
  getExpenses(
    amount: number,
    maxTaxFree = TAX_FREE_VALUE,
    taxRate = TAX_RATE,
  ): ExpensesModel {
    const taxFree = Math.min(maxTaxFree, amount);
    const net = Math.max(0, amount - maxTaxFree);
    const taxes = parseFloat((net * taxRate).toFixed(2));
    const total = taxFree + net + taxes;
    return { taxFree, net, taxes, total };
  }
}

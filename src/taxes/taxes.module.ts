import { Module } from '@nestjs/common';
import { TaxesService } from './taxes.service';

/**
 * Taxes and rates manager
 */
@Module({
  providers: [TaxesService],
  exports: [TaxesService],
})
export class TaxesModule {}

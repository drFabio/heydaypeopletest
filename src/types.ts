export interface Employee {
  id: number;
  name: string;
  budget: number;
  companyId: number;
}

export interface Company {
  id: number;
  title: string;
}

export interface Order {
  id: number;
  date: Date;
  employeeId: number;
  voucherId: number;
}

export interface Voucher {
  id: number;
  amount: number;
  partnerId: number;
}

export interface Partner {
  id: number;
  name: string;
}

export type ListEmployeeArgs = {
  companyId: number;
  month?: number;
  year?: number;
  minRemainingBudget?: number;
};

export type BudgetData = { remaining: number; expent: number };

export interface IDable {
  id: any;
}

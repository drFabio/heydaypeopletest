import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { getApolloServer } from '@nestjs/graphql';
import { gql } from '@apollo/client';
import {
  createTestClient,
  ApolloServerTestClient,
} from 'apollo-server-testing';
import { ApolloServerBase } from 'apollo-server-core';

import { AppModule } from '@src/app.module';
import {
  DEFAULT_REMAINING_BUDGET,
  TAX_FREE_VALUE,
  TAX_RATE,
} from '../src/constants';

describe('GraphQL', () => {
  let app: INestApplication;
  let testingModule: TestingModule;
  let apolloServer: ApolloServerBase;
  let apolloClient: ApolloServerTestClient;

  beforeAll(async () => {
    testingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = testingModule.createNestApplication();
    await app.init();

    apolloServer = getApolloServer(testingModule);
    apolloClient = createTestClient(apolloServer);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('/graphql query.appInfo', () => {
    const query = gql`
      query AppInfo {
        appInfo
      }
    `;

    it('returns the app info', async () => {
      const expectedResult = {
        appInfo: 'heyday tech challenge starter',
      };

      const response = await apolloClient.query({ query });

      expect(response.data).toEqual(expectedResult);
    });
  });

  describe('/graphql companiesEmployees', () => {
    it(`Lists the company employees with their budget withing a given month`, async () => {
      const query = gql`
        query test($year: Int!, $month: Int!) {
          companiesEmployees(year: $year, month: $month) {
            id
            title
            employees {
              id
              name
              expentBudget
              remainingBudget
            }
          }
        }
      `;
      const response = await apolloClient.query({
        query,
        variables: { year: 2020, month: 1 },
      });
      response.data.companiesEmployees.forEach((companyData: any) => {
        companyData.employees.forEach(
          (employee: { remainingBudget: number }) => {
            expect(employee.remainingBudget).toBeGreaterThanOrEqual(
              DEFAULT_REMAINING_BUDGET,
            );
          },
        );
      });
    });
  });

  describe('/graphql employees', () => {
    it(`Lists the employees of a single company with their taxes`, async () => {
      const query = gql`
        query test($year: Int!, $month: Int!, $companyId: Int!) {
          employees(year: $year, month: $month, companyId: $companyId) {
            id
            name
            budget
            expentBudget
            remainingBudget
            expenses {
              net
              taxes
              taxFree
              total
            }
          }
        }
      `;
      const response = await apolloClient.query({
        query,
        variables: { year: 2020, month: 1, companyId: 3 },
      });

      response.data.employees.forEach((employeeData: any) => {
        const expenses: Record<string, number> = employeeData.expenses;
        const { net, taxes, taxFree, total } = expenses;
        expect(taxFree).toBeLessThanOrEqual(TAX_FREE_VALUE);
        expect(total).toEqual(net + taxes + taxFree);
        expect(taxes).toBe(parseFloat((TAX_RATE * net).toFixed(2)));
      });
    });
  });
  describe('/graphql partners', () => {
    it(`Lists the partners revenue within a range`, async () => {
      const query = gql`
        query test($startDate: String!, $endDate: String!) {
          partners {
            id
            name
            revenue(startDate: $startDate, endDate: $endDate) {
              total
            }
          }
        }
      `;
      const response = await apolloClient.query({
        query,
        variables: { startDate: '1999-01-01', endDate: '2222-01-01' },
      });
      response.data.partners.forEach(
        (partnerData: { revenue: { total: number } }) => {
          expect(partnerData.revenue.total).toBeGreaterThan(0);
        },
      );
    });
  });
});
